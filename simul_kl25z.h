/* Simple simulator for Kinetis KL25Z MCU
 * Main include file.
 * Include just this file in your source.
 * Requires C++11 support.
 *
 * Note: to simulate switch input use pinRead() with SW1 - SW4;
 * Keyboard key '1' is SW1, SW2 is key '2' etc.
 * Pressing a key toggles the switch On/Off.
 * So use '1' and '1' to simulate switch SW1 press and release.
 * This is because holding the key down results in the char being sent
 * repeatedly to the program - keyboard has autorepeat feature.
*/
#ifndef SIMUL_KL25Z_H
#define SIMUL_KL25Z_H

#include <cstdint>

// Note: Include simulator headers at the end of this file
// because they need definitions provided here...

#include "simul_regs.h"

// Version of the simulator
#define     VERSION_MAJOR       0
#define     VERSION_MINOR       2

// Enable diagnostic messages
#define    VERBOSE_INFO     0

#if VERBOSE_INFO == 1
    #define PRINT_DGMSG(a) std::cout << a << std::endl
#else
    #define PRINT_DGMSG(a)
#endif

// Global functions
void delay(unsigned int mseconds);
void delay(void);

class SIM_Peripheral;
class PORT_Peripheral;
class ADC_Peripheral;

extern SIM_Peripheral* SIM;
extern PORT_Peripheral* PORTC;
extern ADC_Peripheral* ADC0;

// Universal property with value change handler
// from https://stackoverflow.com/questions/9144819/c-function-calling-when-changing-member-value
#include <functional>

template<typename T>
class Property {
    friend class ADC_Peripheral;

public:
    Property(std::function<void(T)> callback) : data(), callback(callback) { }

    Property& operator=(const T& newvalue) {
        data = newvalue;
        callback(data);

        return *this;
    }

    Property& operator|=(const T& newvalue) {
        data |= newvalue;
        callback(data);
        return *this;
    }

    operator T() const {
        return data;
    }

private:
    T data;
    std::function<void(T)> callback;

};

// SIM module
class SIM_Peripheral {
public:
    uint32_t SCGC5;
    uint32_t SCGC6;
};

// Port module
class PORT_Peripheral {
public:
    uint32_t PCR[32];   // pin control register
};

// simulate systick driver functions
// Functions implemented in simul_kl25z.cpp
void SYSTICK_initialize(void);
uint32_t SYSTICK_millis(void);
uint32_t SYSTICK_micros(void);
void SYSTICK_delay_ms(uint32_t millis);

// include header for simulated ADC
#include "simul_adc.h"
#include "simul_drv_gpio.h"
#include "simul_drv_lcd.h"


#endif // SIMUL_KL25Z_H
