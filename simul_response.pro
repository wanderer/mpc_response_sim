TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    simul_kl25z.cpp

HEADERS += \
    simul_adc.h \
    simul_drv_gpio.h \
    simul_drv_lcd.h \
    simul_kl25z.h \
    simul_regs.h

LIBS += -lpthread
